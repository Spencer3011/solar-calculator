USE [RDWH]
GO
/****** Object:  StoredProcedure [dbo].[SPH_SOLAR_BATT_ANALYSIS]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Spencer Wilson
-- Create date: 5/16/2022
-- Description:	This procedure is used to calculate electricity consumption and price based off of solar radiance data. 
-- The end result is TOU Rate with Solar/Battery and the adjusted consumption per hour (accounting for solar+battery).
-- =============================================
ALTER PROCEDURE [dbo].[SPH_SOLAR_BATT_ANALYSIS]
	-- Add the parameters for the stored procedure here
	@Location AS bigint,
	@Battery_Size AS DECIMAL(4,2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Step 1. Unpivot Meter Data table to aid in calculations
	;WITH [Meter Data] AS (
		SELECT *
		FROM 
			(SELECT Read_Date, ISNULL(Hour01,0)Hour01, ISNULL(Hour02,0)Hour02, ISNULL(Hour03,0)Hour03, ISNULL(Hour04,0)Hour04, 
				ISNULL(Hour05,0)Hour05, ISNULL(Hour06,0)Hour06, ISNULL(Hour07,0)Hour07, ISNULL(Hour08,0)Hour08, ISNULL(Hour09,0)Hour09, ISNULL(Hour10,0)Hour10, 
				ISNULL(Hour11,0)Hour11, ISNULL(Hour12,0)Hour12, ISNULL(Hour13,0)Hour13, ISNULL(Hour14,0)Hour14, ISNULL(Hour15,0)Hour15, ISNULL(Hour16,0)Hour16,
				ISNULL(Hour17,0)Hour17, ISNULL(Hour18,0)Hour18, ISNULL(Hour19,0)Hour19, ISNULL(Hour20,0)Hour20, ISNULL(Hour21,0)Hour21,
				ISNULL(Hour22,0)Hour22, ISNULL(Hour23,0)Hour23, ISNULL(Hour24,0)Hour24
			FROM [METER_DATA].[dbo].[Converted_Meter_Data]
			WHERE [Location] = @Location) AS pvt
		UNPIVOT
			([Value] FOR [Hour] IN
				(Hour01, Hour02, Hour03, Hour04, Hour05, Hour06, Hour07, Hour08, Hour09, Hour10, Hour11, Hour12, Hour13, Hour14, Hour15, Hour16, Hour17, Hour18, Hour19, Hour20, Hour21,
				Hour22, Hour23, Hour24)
			) AS unpvt
	),

	--Step 2. Combine unpivoted meter data with Solar + Battery tables utilzing an inner join
	[Raw Data] AS (
		SELECT 
		Solar_Unpvt.[Date] AS Solar_Date, 
		Solar_Unpvt.[Hour] AS Solar_Hour, 
		Solar_Unpvt.[Value] AS Solar_Value,
		Solar_Unpvt.[Array_Size] AS Solar_Size,
		Batt.[Date] as Batt_Date,
		Batt.[Hour] as Batt_Hour,
		Batt.Battery_Capacity AS Batt_Cap,
		Batt.Battery_Capacity2 AS Batt_Cap2,
		Batt.EV_Charging_Estimation as Batt_EV,
		md.[Read_Date] AS Meter_Date,
		md.[Hour] AS Meter_Hour,
		md.[Value] AS Meter_Value,
		Solar_Unpvt.[Array_Size]
		FROM [RDWH].[dbo].[NREL_Solar_Radiance_Data_Unpivot] AS Solar_Unpvt
		INNER JOIN [Meter Data] md ON md.Read_Date = Solar_Unpvt.[Date] AND md.[Hour] = Solar_Unpvt.[Hour]
		INNER JOIN [RDWH].[dbo].Battery_Capacity Batt ON Batt.[Date] = Solar_Unpvt.[Date] AND Batt.[Hour] = Solar_Unpvt.[Hour]
	),

	--Step 3. Establish a season for each date to determine the correct rate to use. Two different columns to distinguish between A27 and A27TOU rates.
	-- Also create an 'Adjusted_Usage' variable to store usage with solar/battery accounted for. This allows us to have correct calculations for a running total for the standard A27 rate.
	[Season] AS (
		SELECT 
		CASE
			WHEN Solar_Size = 0 THEN Meter_Value
			ELSE Meter_Value - Solar_Value + (Batt_Cap * @Battery_Size)
		END AS Adjusted_Usage,
		CASE
			WHEN Solar_Size = 0 THEN Meter_Value + Batt_EV
			ELSE Meter_Value - Solar_Value + (Batt_Cap2 * @Battery_Size) + Batt_EV
		END AS Adjusted_Usage2,
		Batt_Cap, Solar_Date, Solar_Hour, Batt_Date, Batt_Hour, Meter_Date, Meter_Hour, Meter_Value, Solar_Value, Solar_Size,
		CASE
			WHEN DATEPART(MONTH, Solar_Date) < 4 THEN 'Winter'
			WHEN DATEPART(MONTH, Solar_Date) > 10 THEN 'Winter'
			WHEN DATEPART(MONTH, Solar_Date) = 4 AND DATEPART(DAY, Solar_Date) < 16 THEN 'Winter'
			WHEN DATEPART(MONTH, Solar_Date) = 10 AND DATEPART(DAY, Solar_Date) > 15 THEN 'Winter'
			ELSE 'Summer'
		END AS [Season_TOU],
		CASE
			WHEN DATEPART(MONTH, Solar_Date) < 6 THEN 'Winter'
			WHEN DATEPART(MONTH, Solar_Date) > 10 THEN 'Winter'
			ELSE 'Summer'
		END AS [Season_A27]
	FROM [Raw Data]
	),

	--Step 4. When a solar credit is applied adjusted value is set to 0. This is because the running total of usage should not be offset by solar credits.
	[RunningCalc] AS (
	SELECT Adjusted_Usage, Adjusted_Usage2, Solar_Date, Solar_Hour, Season_TOU, Season_A27, Solar_Size, Meter_Value, Batt_Cap, Solar_Value,
	CASE	
		WHEN Adjusted_Usage < 0 THEN 0
		ELSE Adjusted_Usage
	END AS [Adjusted]
	FROM [Season]
	),

	--Step 5. Establish which hours are 'ON' and 'OFF' peak to aid calculations. This is needed for all solar rates and for TOU rate.
	--Also create an updating sum that takes in 'adjusted' and sums it over each month creating a live total.
	[Peak] AS (
	SELECT Adjusted_Usage, Adjusted_Usage2, Solar_Date, Solar_Hour, Season_TOU, Season_A27, Solar_Size, Meter_Value, Batt_Cap, Solar_Value,
	SUM(Adjusted) OVER (PARTITION BY DATEPART(MONTH, Solar_Date), DATEPART(YEAR, Solar_Date),Solar_Size ORDER BY Solar_Date, Solar_Hour) AS UpdatingSum,
	CASE
		WHEN [Season_TOU] = 'Winter' AND ([Solar_Hour] = 'Hour07' OR Solar_Hour = 'Hour08') THEN 'ON'
		WHEN [Season_TOU] = 'Summer' AND (Solar_Hour = 'Hour16' OR Solar_Hour = 'Hour17' OR Solar_Hour = 'Hour18') THEN 'ON'
		ELSE 'OFF'
	END AS [ON/OFF PEAK],
	CASE
		WHEN [Season_TOU] = 'Winter' AND ([Solar_Hour] = 'Hour07' OR Solar_Hour = 'Hour08') THEN 'ON'
		WHEN [Season_TOU] = 'Summer' AND (Solar_Hour = 'Hour16' OR Solar_Hour = 'Hour17' OR Solar_Hour = 'Hour18') THEN 'ON'
		WHEN [Solar_Hour] = 'Hour22' OR [Solar_Hour] = 'Hour23' OR [Solar_Hour] = 'Hour24' OR [Solar_Hour] = 'Hour01' OR [Solar_Hour] = 'Hour02' OR [Solar_Hour] = 'Hour03'
		OR [Solar_Hour] = 'Hour04' OR [Solar_Hour] = 'Hour05' THEN 'SUPEROFF'
		ELSE 'OFF'
	END AS [SUPER ON/OFF PEAK]
	FROM [RunningCalc]
	),

	--Step 6. Perform calculations on 'Adjusted Usage' to determine cost of power for a given hour. Two different case statements for A27 and A27TOU
	[Cost] AS (
	SELECT  Adjusted_Usage, Adjusted_Usage2, Solar_Date, Solar_Hour, Season_TOU, Season_A27, [ON/OFF PEAK], [SUPER ON/OFF PEAK], Solar_Size, Meter_Value, Batt_Cap, UpdatingSum, Solar_Value,
	CASE 
		WHEN  Adjusted_Usage > 0 AND [ON/OFF PEAK] = 'ON' THEN  Adjusted_Usage * 0.4785
		WHEN  Adjusted_Usage < 0 AND [ON/OFF PEAK] = 'ON' THEN  Adjusted_Usage * 0.14711
		WHEN  Adjusted_Usage > 0 AND [ON/OFF PEAK] = 'OFF' THEN  Adjusted_Usage * 0.0563
		WHEN  Adjusted_Usage < 0 and [ON/OFF PEAK] = 'OFF' THEN  Adjusted_Usage * 0.02902
	END AS [TOU Price],
	CASE
		WHEN Adjusted_Usage < 0 AND [ON/OFF PEAK] = 'ON' THEN Adjusted_Usage*0.14711
		WHEN Adjusted_Usage < 0 AND [ON/OFF PEAK] = 'OFF' THEN Adjusted_Usage*0.02902		
		WHEN Season_A27 = 'Winter' AND UpdatingSum <= 1000 THEN Adjusted_Usage*0.1145
		WHEN Season_A27 = 'Winter' AND UpdatingSum > 1000 AND UpdatingSum < 3000 THEN Adjusted_Usage*0.1079
		WHEN Season_A27 = 'Winter' AND UpdatingSum > 3000 THEN Adjusted_Usage*0.0950
		WHEN Season_A27 = 'Summer' AND UpdatingSum <= 3000 THEN Adjusted_Usage*0.1192
		WHEN Season_A27 = 'Summer' AND UpdatingSum > 3000 THEN Adjusted_Usage*0.1133
	END AS [A27 Price],
	CASE
		WHEN Adjusted_Usage2 < 0 AND [SUPER ON/OFF PEAK] = 'ON' THEN Adjusted_Usage2*0.14711
		WHEN Adjusted_Usage2 < 0 AND [SUPER ON/OFF PEAK] = 'OFF' THEN Adjusted_Usage2*0.02902 
		WHEN Adjusted_Usage2 > 0 AND [SUPER ON/OFF PEAK] = 'ON' THEN Adjusted_Usage2*0.3747
		WHEN Adjusted_Usage2 > 0 AND [SUPER ON/OFF PEAK] = 'OFF' THEN Adjusted_Usage2*0.0867
		WHEN Adjusted_Usage2 >= 0 AND [SUPER ON/OFF PEAK] = 'SUPEROFF' THEN Adjusted_Usage2*0.0311
	END AS [TOU PEV Price]
	FROM [Peak]
	), 

	--Step 7. Take the calculations generated by the previous CTE and sum them over each month, making sure that it resets monthly. This is the total monthly cost.
	--Added a case statement to determine basic facilities charge. If no solar, then the cost should reflect that. 
	[Cost AGG] AS (
	SELECT Adjusted_Usage, Adjusted_Usage2, Solar_Date, Solar_Hour, Season_TOU, Season_A27, [ON/OFF PEAK], Solar_Size, Meter_Value, Batt_Cap, UpdatingSum, Solar_Value, [A27 Price],
	SUM(ROUND([TOU Price],3)) OVER (PARTITION BY DATEPART(MONTH, Solar_Date), DATEPART(YEAR, Solar_Date),Solar_Size) + 32.50 +
	CASE
		WHEN Solar_Size = 0 THEN 0
		WHEN @Battery_Size > 0 THEN 12
		ELSE 10
	END AS [TOU Cost],
	SUM(ROUND([A27 Price],3)) OVER (PARTITION BY DATEPART(MONTH, Solar_Date), DATEPART(YEAR, Solar_Date),Solar_Size) + 30 +
	CASE
		WHEN Solar_Size = 0 THEN 0
		WHEN @Battery_Size > 0 THEN 12
		ELSE 10
	END AS [A27 Cost],
	SUM(ROUND([TOU PEV Price],3)) OVER (PARTITION BY DATEPART(MONTH, Solar_Date), DATEPART(YEAR, Solar_Date),Solar_Size) + 32.50 +
	CASE 
		WHEN Solar_Size = 0 THEN 0
		WHEN @Battery_Size > 0 THEN 12
		ELSE 10
	END AS [TOU PEV COST]
	FROM [Cost]
	)

	SELECT 
	Solar_Date, Solar_Hour, Season_A27, Season_TOU, [ON/OFF PEAK], Solar_Size, [TOU Cost], [A27 Cost],[TOU PEV COST], Meter_Value, Solar_Value, Adjusted_Usage, Adjusted_Usage2, [A27 Price]
	FROM [Cost AGG]
	ORDER BY Solar_Size, Solar_Date, Solar_Hour


END
